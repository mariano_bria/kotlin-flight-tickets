package com.example.flighttickets.configuration

import com.example.flighttickets.data.IFlightTicketsRepository
import com.example.flighttickets.data.entities.FlightTicket
import org.springframework.boot.ApplicationRunner
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.time.LocalDate
import java.time.LocalTime

@Configuration
class FlightTicketsConfig {

    @Bean
    fun databaseInitializer(repository: IFlightTicketsRepository) = ApplicationRunner {

        repository.save(FlightTicket(
                itineraryId = "PNR1234",
                departureDate = LocalDate.now(),
                arrivalDate = LocalDate.now().plusWeeks(1),
                cityOfOrigin = "Cordoba",
                destinationCity = "Los Angeles",
                passengerName = "John Smith",
                passengerAge = 30,
                hasLuggageStored = true,
                price = 888.88,
                departureTime = LocalTime.now(),
                arrivalTime = LocalTime.now().plusHours(5)
        ))
        repository.save(FlightTicket(
                itineraryId = "PNR2112",
                departureDate = LocalDate.now().plusDays(10),
                arrivalDate = LocalDate.now().plusDays(20),
                cityOfOrigin = "Dallas",
                destinationCity = "Houston",
                passengerName = "Homer Simpson",
                passengerAge = 57,
                hasLuggageStored = false,
                price = 152.00,
                departureTime = LocalTime.now(),
                arrivalTime = LocalTime.now().plusHours(5)
        ))
    }
}