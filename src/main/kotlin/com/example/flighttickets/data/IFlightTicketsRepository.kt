package com.example.flighttickets.data

import com.example.flighttickets.data.entities.FlightTicket
import org.springframework.data.repository.CrudRepository

interface IFlightTicketsRepository : CrudRepository<FlightTicket, Long> {
    fun findByItineraryId(itineraryId: String): FlightTicket?

    fun save(flightTicket: FlightTicket) {
        this.save(flightTicket);
    }
}