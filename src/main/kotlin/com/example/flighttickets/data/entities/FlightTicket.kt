package com.example.flighttickets.data.entities

import org.springframework.format.annotation.DateTimeFormat
import java.time.LocalDate
import java.time.LocalTime
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
class FlightTicket(
        var itineraryId: String?,
        var departureDate: LocalDate?,
        var arrivalDate: LocalDate?,
        var cityOfOrigin: String,
        var destinationCity: String,
        var passengerName: String,
        var passengerAge: Int,
        var hasLuggageStored: Boolean?,
        var price: Double,
        var departureTime: LocalTime?,
        var arrivalTime: LocalTime?,
        @Id @GeneratedValue var id: Long? = null
)