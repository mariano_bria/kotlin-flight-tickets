package com.example.flighttickets

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class FlightTicketsApplication

fun main(args: Array<String>) {
	runApplication<FlightTicketsApplication>(*args)
}
