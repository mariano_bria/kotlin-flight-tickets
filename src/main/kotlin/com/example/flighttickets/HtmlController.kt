package com.example.flighttickets

import com.example.flighttickets.controllers.TicketController
import com.example.flighttickets.data.entities.FlightTicket
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.set
import org.springframework.web.bind.annotation.*
import java.time.LocalDate
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import javax.validation.Valid

const val STRING_LENGTH = 7;

@Controller
class HtmlController(private val controller: TicketController) {

    @GetMapping("/")
    fun tickets(model: Model): String {
        model["title"] = "Flight Tickets List"
        model["flightTicketsList"] = controller.findAll()

        return "tickets"
    }

    @GetMapping("/tickets/{itineraryId}")
    fun showTicket(model: Model, @PathVariable itineraryId: String): String? {
        model["title"] = "Flight Ticket"
        model["flightTicket"] = controller.findTicketByItineraryId(itineraryId)
        return "flight-ticket"
    }

    @GetMapping("/add")
    fun showAddTicket(model: Model): String? {
        return "add-ticket"
    }

    @PostMapping("/add")
    fun addTicket(model: Model,
                @Valid @ModelAttribute("flightTicket") flightTicket: FlightTicket,
                @RequestParam("departureDateString") departureDateString: String,
                @RequestParam("arrivalDateString") arrivalDateString: String,
                @RequestParam("hasLuggageStoredCB") hasLuggageStoredCB: String): String? {
        return try {
            flightTicket.itineraryId = createRandomItineraryId()
            flightTicket.departureDate = LocalDate.parse(departureDateString, DateTimeFormatter.ISO_DATE)
            flightTicket.arrivalDate = LocalDate.parse(arrivalDateString, DateTimeFormatter.ISO_DATE)
            flightTicket.hasLuggageStored = hasLuggageStoredCB != "false"
            flightTicket.departureTime = LocalTime.now()
            flightTicket.arrivalTime = LocalTime.now().plusHours(5)

            val newFlightTicket: Unit = controller.createFlightTicket(flightTicket)
            "redirect:/"
        } catch (ex: Exception) {
            val errorMessage = ex.message
            model.addAttribute("errorMessage", errorMessage)
            model.addAttribute("add", true)
            "/"
        }
    }

    fun createRandomItineraryId(): String? {
        val charPool : List<Char> = ('A'..'Z') + ('0'..'9')
        val randomString = (1..STRING_LENGTH)
                .map { i -> kotlin.random.Random.nextInt(0, charPool.size) }
                .map(charPool::get)
                .joinToString("");

        return randomString
    }
}
