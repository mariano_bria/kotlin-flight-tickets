package com.example.flighttickets.controllers

import com.example.flighttickets.data.IFlightTicketsRepository
import com.example.flighttickets.data.entities.FlightTicket
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException

@RestController
@RequestMapping("/api/ticket")
class TicketController(private val repository: IFlightTicketsRepository) {

    @GetMapping("/")
    fun findAll(): Iterable<FlightTicket> = repository.findAll()

    @GetMapping("/{itineraryId}")
    fun findTicketByItineraryId(@PathVariable itineraryId: String) =
            repository.findByItineraryId(itineraryId) ?:
            ResponseStatusException(HttpStatus.NOT_FOUND, "This ticket does not exist")

    @PostMapping("/add")
    fun createFlightTicket(@RequestBody flightTicketRequest: FlightTicket) {
        return repository.save(flightTicketRequest)
    }
}